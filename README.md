# Hello World in Bitbucket

Follow tutorial [Learn Continuous Integration with Bitbucket Pipelines](https://www.atlassian.com/continuous-delivery/tutorials/continuous-integration-tutorial)

## Simple Express HelloWorld app

```powershell
mkdir helloworld
cd helloworld
npm init
```


## Add test

```powershell
npm install mocha --save-dev
npm install supertest --save-dev  
npm install eslint --save-dev
```

```
eslint --init
```

